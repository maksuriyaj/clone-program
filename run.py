import zipfile
import csv
from urllib import request
import xml.etree.ElementTree as ET

def bitbucket(username, repoName, writer):
    
    zipName = "master.zip"

    url = "https://bitbucket.org/%s/%s/get/master.zip" % (username, repoName)
    
    try:
        request.urlretrieve(url, zipName)
        with zipfile.ZipFile(zipName, "r") as zfile:
            zfile.extractall()
   
        zf = zipfile.ZipFile(zipName, "r")
        folderName = zf.namelist()[0]
        renameProject(folderName, username)

        writer.writerow([username,"ok"])
        print(username+" ok")
    except request.URLError as e:
        if e.code == 404:
            writer.writerow([username, "โหลดไม่ได้ repo ว่าง"])
            print(username+" empty repo")
        if e.code == 401:
            writer.writerow([username, "โหลดไม่ได้ เอาติ๊ก private repository ออก"])
            print(username+" private repository")
        if e.code == 500:
            writer.writerow([username, "โหลดไม่ได้ ยังไม่ได้สร้าง repo ใน bitbucket"])
            print(username+" repo not exist")

def renameProject(folderName,username):
    tree = ET.parse(folderName+".project")
    root = tree.getroot()
    name = root.find("name")
    name.text = username
    tree.write(folderName+".project")
    
with open("usernames.txt", "r") as ins:
    usernames = []
    for line in ins:
        line = line.replace("\n","")
        line = line.replace(",","")
        usernames.append(line)
    #print(usernames)
    with open("status.csv", "w", newline='') as f:
        writer = csv.writer(f)
        c = 1
        for username in usernames:
            print(str(c)+". ", end="")
            bitbucket(username, "sc-lab-01", writer)
            c += 1
    print("finish")

